import logging, os, base64
from cryptography.hazmat.primitives.serialization import (
    pkcs12,
    Encoding,
    PrivateFormat,
    NoEncryption
)
from azure.core.exceptions import ServiceRequestError, ResourceNotFoundError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "get-certificate" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None: 
        logger.info(f'User {user} called `get-certificate`', extra=logger_properties)
    else:
        logger.warning(f'No header was given for user', extra=logger_properties)

    # Read any route parameters
    vault_url   = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    name        = req.route_params.get('name')
    include_key = req.params.get('include_key')

    # Create a client to interact with the Key Vault
    #
    # We use the Secret store to retrieve both the certificate and the private key,
    # in case the user wants it.
    client = SecretClient(vault_url, DefaultAzureCredential())

    # Retrieve the certificate
    try:
        secret = client.get_secret(name)
        # Get the cert and key from the secret
        key, cert, _ = pkcs12.load_key_and_certificates(base64.b64decode(secret.value), None)
        cert = (cert.public_bytes(encoding=Encoding.PEM)).decode()
        # Decode the key if the user wants it
        if include_key == '1':
            key = (key.private_bytes(
                encoding=Encoding.PEM,
                format=PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=NoEncryption())
            ).decode()
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    except ResourceNotFoundError:
        return HttpResponse(f"Certificate ({name}) not found", status_code=404)
    
    return HttpResponse(f"{key if include_key == '1' else ''}{cert}")
