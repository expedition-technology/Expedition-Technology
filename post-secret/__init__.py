import logging, os
from datetime import datetime
from secrets import choice
from string import ascii_letters, digits
from azure.core.exceptions import ServiceRequestError, ResourceExistsError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "post-secret" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None:
        logger.info(f'User {user} called `post-secret`', extra=logger_properties)
    else: 
        logger.error('No header was given for user', extra=logger_properties)
        return HttpResponse("Aborting due to incomplete headers", status_code=406)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"

    # Read body
    try:
        req_body = req.get_json()
    except ValueError:
        pass
    else:
        name       = req_body.get('name')
        value      = req_body.get('value')
        properties = req_body.get('properties')

    # Body Validation
    if name is None or name == "":
        return HttpResponse('No valid secret name not found in body', status_code=400)

    # Create a client to interact with the Key Vault
    client = SecretClient(vault_url, DefaultAzureCredential())

    # Create a random value if none is given
    if not value or value == "":
        value = ''.join(choice(ascii_letters + digits) for i in range(16))
    
    # Convert strings to datetime objects
    try:
        properties['expires_on'] = datetime.strptime(properties.get('expires_on'), "%Y-%m-%d %H:%M:%S")
        properties['not_before'] = datetime.strptime(properties.get('not_before'), "%Y-%m-%d %H:%M:%S")
    except (KeyError, TypeError, AttributeError):
        # We just assume it isn't given in the body
        pass

    # Create the secret
    try:
        client.set_secret(name, value)
        if properties is not None:
            client.update_secret_properties(name,
                expires_on=properties.get('expires_on'),
                not_before=properties.get('not_before'),
                tags=properties.get('tags')
            )
        logger.info(f"User {user} created/updated secret {name}", extra=logger_properties)
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    except ResourceExistsError:
        return HttpResponse('Secret exists, but in a deleted state', status_code=403)

    return HttpResponse(status_code=201)
