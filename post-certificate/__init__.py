import logging, os, asyncio
from azure.core.exceptions import ServiceRequestError, ResourceExistsError
from azure.functions import HttpRequest, HttpResponse
from azure.identity.aio import DefaultAzureCredential
from azure.keyvault.certificates.aio import CertificateClient
from azure.keyvault.certificates import CertificatePolicy
from opencensus.ext.azure.log_exporter import AzureEventHandler

async def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "post-certificate" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None:
        logger.info(f'User {user} called `post-certificate`', extra=logger_properties)
    else: 
        logger.error('No header was given for user', extra=logger_properties)
        return HttpResponse("Aborting due to incomplete headers", status_code=406)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"

    # Read body
    try:
        req_body = req.get_json()
    except ValueError:
        return HttpResponse('No valid body', status_code=400)
    else:
        name       = req_body.get('name')
        properties = req_body.get('properties')
        policy     = req_body.get('policy')

    # Body Validation
    if name is None or name == "":
        return HttpResponse('No certificate name found in body', status_code=400)
    if policy is None or policy == "":
        return HttpResponse('No policy found in body', status_code=400)
    
    # If the subject doesn't start with 'CN=', we add it ourselves
    if not policy.get('subject').startswith('CN='):
        policy['subject'] = f"CN={policy.get('subject')}"

    # Create a client to interact with the Key Vault
    credential = DefaultAzureCredential()
    client = CertificateClient(vault_url, credential)

    # Create the certificate
    try:
        cert_policy = CertificatePolicy(
            issuer_name=policy.get('issuer_name'),
            
            subject=policy.get('subject'),
            san_dns_names=policy.get('san_dns_names').split(',') if policy.get('san_dns_names') is not None else None,
            san_emails=policy.get('san_emails').split(',') if policy.get('san_emails') is not None else None,
            san_user_principal_names=policy.get('san_user_principal_names').split(',') if policy.get('san_user_principal_names') is not None else None,
            
            key_type=policy.get('key_type') if policy.get('key_type') is not None else 'RSA',
            key_size=int(policy.get('key_size')) if policy.get('key_size') is not None else 2048,
            key_curve_name=(policy.get('key_curve_name') if policy.get('key_curve_name') is not None else 'P-256') if policy.get('key_type') == 'EC' else None,
            reuse_key=bool(policy.get('reuse_key')) if policy.get('reuse_key') is not None else None,
            key_usage=policy.get('key_usage').split(',') if policy.get('key_usage') is not None else None,
            enhanced_key_usage=policy.get('enhanced_key_usage').split(',') if policy.get('extended_key_usage') is not None else None,
            validity_in_months=int(policy.get('validity_in_months')) if policy.get('validity_in_months') is not None else 12
        )
        # Create the certificate and update any properties if given
        await client.create_certificate(name, cert_policy)
        if properties is not None:
            client.update_certificate_properties(
                name,
                tags=properties.get('tags')
            )
        logger.info(f"User {user} created/updated certificate {name}", extra=logger_properties)
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    except ResourceExistsError:
        return HttpResponse('Certificate exists, but in a deleted state', status_code=403)
    finally: 
        await credential.close()
        await client.close()
    return HttpResponse(status_code=201)
