import logging, os, json
from azure.core.exceptions import ServiceRequestError, ResourceNotFoundError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "list-secrets" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None: 
        logger.info(f'User {user} called `list-secrets`', extra=logger_properties)
    else:
        logger.warning('No header was given for user', extra=logger_properties)
        return HttpResponse("Aborting due to incomplete headers", status_code=406)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    include_deleted = req.params.get("include_deleted")

    # Create a client to interact with the Key Vault
    client = SecretClient(vault_url, DefaultAzureCredential())

    try:
        secrets = client.list_properties_of_secrets()
        if include_deleted is not None:
            deleted_secrets = client.list_deleted_secrets()
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection/request", status_code=408)
    
    # Retrieve the secrets
    output = {'secrets': []}
    for secret in secrets:
        if secret.content_type is None:
            output['secrets'].append(secret.name)
    if include_deleted is not None:
        output.update({'deleted-secrets': []})
        for deleted_secret in deleted_secrets:
            output['deleted-secrets'].append(deleted_secret.name)
    return HttpResponse(json.dumps(output, indent=2, sort_keys=True), mimetype="application/json")
