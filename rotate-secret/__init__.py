import logging, os
from secrets import choice
from string import ascii_letters, digits
from azure.core.exceptions import ServiceRequestError, ResourceExistsError
from azure.functions import EventGridEvent
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(event: EventGridEvent):
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "rotate-secret" }}

    # Get name of vault and object from the event
    vault_url = f"https://{event.get_json().get('VaultName')}.vault.azure.net"
    name = event.get_json().get('ObjectName')
    # Generate a new secret
    value = ''.join(choice(ascii_letters + digits) for i in range(16))
    
    logger.info(f'Event triggered for expiration of secret `{name}`', extra=logger_properties)

    # Create a client to interact with the Key Vault
    client = SecretClient(vault_url, DefaultAzureCredential())

    # Update the secret
    try:
        client.set_secret(name, value)
    except ServiceRequestError:
        logging.error("Couldn't connect to Azure Key Vault, check credentials/connection")
    except ResourceExistsError:
        logging.error('Secret exists, but in a deleted state')

    logger.info(f'Secret `{name}` updated with new value', extra=logger_properties)
