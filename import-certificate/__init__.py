import logging, os
from OpenSSL.crypto import (
    load_certificate,
    load_privatekey,
    FILETYPE_PEM,
    PKCS12
)
from azure.core.exceptions import ServiceRequestError, ResourceExistsError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.certificates import CertificateClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "import-certificate" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None: 
        logger.info(f'User {user} called `import-certificate`', extra=logger_properties)
    else:
        logger.error('No header was given for user', extra=logger_properties)
        return HttpResponse('Aborting due to incorect headers', status_code=406)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    name = req.route_params.get('name')
    passphrase = req.params.get('passphrase')

    # Read the body
    try:
        body = req.get_body()
    except ValueError:
        return HttpResponse("Couldn't read body", status_code=400)

    # Split our body into 2 strings
    key  = body[body.find(bytes('-----BEGIN RSA PRIVATE KEY-----', 'UTF-8')):body.find(bytes('-----END RSA PRIVATE KEY-----', 'UTF-8')) + 29]
    cert = body[body.find(bytes('-----BEGIN CERTIFICATE-----', 'UTF-8')):body.find(bytes('-----END CERTIFICATE-----', 'UTF-8')) + 25]

    # Convert strings to x509 objects
    cert = load_certificate(FILETYPE_PEM, cert)
    key  = load_privatekey(FILETYPE_PEM, key, passphrase=passphrase)
    
    # Convert x509 objects to a PKCS12 object
    pfx = PKCS12()
    pfx.set_certificate(cert)
    pfx.set_privatekey(key)

    # Create a client to interact with the Key Vault
    client = CertificateClient(vault_url, DefaultAzureCredential())

    # Create the certificate
    try:
        client.import_certificate(name, pfx.export())
        logger.info(f"User {user} imported certificate {name}", extra=logger_properties)
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    except ResourceExistsError:
        return HttpResponse('Certificate exists, but in a deleted state', status_code=403)

    return HttpResponse(status_code=201)
