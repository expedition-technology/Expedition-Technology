import logging, os
from azure.core.exceptions import ServiceRequestError, ResourceNotFoundError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "delete-secret" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None:
        logger.info(f'User {user} called `delete-secret`', extra=logger_properties)
    else: 
        logger.error('No header was given for user', extra=logger_properties)
        return HttpResponse("Aborting due to incomplete headers", status_code=406)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    name = req.route_params.get('name')
    purge = req.params.get("purge")

    # Create a client to interact with the Key Vault
    client = SecretClient(vault_url, DefaultAzureCredential())

    # Delete the secret
    try:
        client.begin_delete_secret(name).wait()
        logger.info(f'User {user} deleted secret {name}', extra=logger_properties)
        if purge == "1":
            client.purge_deleted_secret(name)
            logger.info(f'User {user} purged secret {name}', extra=logger_properties)
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    except ResourceNotFoundError:
        try: 
            if purge == "1":
                client.purge_deleted_secret(name)
                logger.info(f'User {user} purged secret {name}', extra=logger_properties)
            else:
                return HttpResponse(status_code=404)
        except ServiceRequestError:
            return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
        except ResourceNotFoundError:
            return HttpResponse(status_code=404)
    
    return HttpResponse()
