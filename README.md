Code
====

Omschrijving
------------

Project: Microsoft Azure Functions & Key Vault 

Feasability onderzoeken van het inzetten van Azure Functions voor het aanspreken van een Microsoft Key Vault op Azure.

Teamcompositie
--------------

* Sander Nouwen (0126524-36)
* Christophe van der Weiden (0134741-08)
* Arnaud Siebens (0132807-14)
