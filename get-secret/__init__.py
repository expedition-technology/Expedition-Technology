import logging, os
from azure.core.exceptions import ServiceRequestError, ResourceNotFoundError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from datetime import datetime, timedelta
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "get-secret" }}

    # Get user from header
    user = req.headers.get('x-user')
    if user is not None: 
        logger.info(f'User {user} called `get-secret`', extra=logger_properties)
    else:
        logger.warning(f'No header was given for user', extra=logger_properties)

    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    name = req.route_params.get('name')
    expire = req.params.get('expire')

    # Create a client to interact with the Key Vault
    client = SecretClient(vault_url, DefaultAzureCredential())

    # Retrieve the secret
    try:
        secret = client.get_secret(name)
        if expire != '0':
            client.update_secret_properties(name, expires_on=datetime.utcnow() + timedelta(minutes=5))
            logger.info(f"User {user} set secret {name} to expire", extra=logger_properties)
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection/request", status_code=408)
    except ResourceNotFoundError:
        return HttpResponse(f"Secret ({name}) not found", status_code=404)
    else:
        return HttpResponse(secret.value)
