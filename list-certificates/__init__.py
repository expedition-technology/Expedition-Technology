import logging, os, json
from azure.core.exceptions import ServiceRequestError
from azure.functions import HttpRequest, HttpResponse
from azure.identity import DefaultAzureCredential
from azure.keyvault.certificates import CertificateClient
from opencensus.ext.azure.log_exporter import AzureEventHandler

def main(req: HttpRequest) -> HttpResponse:
    # Create logger
    logger = logging.getLogger(__name__)
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={os.getenv("APPINSIGHTS_INSTRUMENTATIONKEY")}'))
    logger.setLevel(logging.INFO)
    logger_properties = { "custom_dimensions": { "operation": "list-certificates" }}

     # Get user from header
    user = req.headers.get('x-user')
    if user is not None: 
        logger.info(f'User {user} called `list-certificates`', extra=logger_properties)
    else:
        logger.error('No header was given for user', extra=logger_properties)
        return HttpResponse("Aborting due to incomplete headers", status_code=406)
    
    # Read any route parameters
    vault_url = f"https://{req.route_params.get('vault_name')}.vault.azure.net"
    include_deleted = req.params.get("include_deleted")
    
    # Create a client to interact with the Key Vault
    client = CertificateClient(vault_url, DefaultAzureCredential())
    
    # Get a list of all certificates from the vault
    try:
        certificates = client.list_properties_of_certificates()
        if include_deleted is not None:
            deleted_certificates = client.list_deleted_certificates()
    except ServiceRequestError:
        return HttpResponse("Couldn't connect to Azure Key Vault, check credentials/connection", status_code=408)
    
    # Retrieve the certificates
    output = {'certificates': []}
    for certificate in certificates:
        output['certificates'].append(certificate.name)
    
    # Include deleted secrets if the user wants it
    if include_deleted is not None:
        output.update({'deleted-certificates': []})
        for deleted_certificate in deleted_certificates:
            output['deleted-certificates'].append(deleted_certificate.name)
    
    return HttpResponse(json.dumps(output, indent=2, sort_keys=True), mimetype="application/json")
